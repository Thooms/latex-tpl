latex-tpl
=========

A few TeX files that I use for my documents. It includes some macros to translates theorem heads or switch between a titlepage or a simple title.